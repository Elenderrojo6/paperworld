﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class Timer : MonoBehaviour
{
   
    public Text timer;
 
    private float firstTime;
 
    // Start is called before the first frame update
    void Start()
    {
        firstTime = Time.time;
    }
 
    void Update(){
        float t = Time.time - firstTime;

        string minuto = ((int) t / 60).ToString("00");
        string segundo = (t % 60).ToString("00");

        timer.text = minuto + ":" + segundo;
        
    }
}