﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoyController : MonoBehaviour{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 4f;
    public float maxSShift = 11f;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    [SerializeField] private Animator anim;
    [SerializeField] private Transform graphics;
    private bool flipped = false;

    private bool jump = false;
    private bool grounded = false;
    private bool run = false;

    private float speed = 0;
    private bool dead = false;

    [SerializeField] GameObject graphic;
    [SerializeField] Collider2D collider;
    [SerializeField] GameObject vida1;
    [SerializeField] GameObject vida2;
    [SerializeField] GameObject vida3;

    public int lives = 3;


    [SerializeField] Text timer;
    private float gameTime;
    
    [SerializeField] AudioSource audioSource;


    // Use this for initialization
    void Awake () {
        rb2d = GetComponent<Rigidbody2D>();
    }
    void Start (){
        gameTime = 0;
        SetTime();
    }

    void Update (){
        if(!dead){
            gameTime += Time.deltaTime;
            SetTime();
    }
    }

    // Update is called once per frame
    void FixedUpdate () {

        if(dead){
            return;
        }
        
        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");

        jump = Input.GetButtonDown("Jump");
        run = Input.GetButton("Run");

        if(run){
            speed = maxSShift + 5;
        }else{
            speed = maxS + 9;
        }

        if(jump && grounded){
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y+speed);
            
            
        }else{
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y);
            
            
        }

        //Ponemos la velocidad horizontal y vertical
        anim.SetFloat("velocityH",Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("velocityV",Mathf.Abs(rb2d.velocity.y));
        anim.SetBool("grounded",grounded);

        if(rb2d.velocity.x<0){
            graphics.transform.localScale = new Vector3(-1,1,1);
        }else{
            graphics.transform.localScale = new Vector3(1,1,1);
        }

        
    }

    private void OnCollisionEnter2D(Collision2D other) {
        grounded = true;
    }

    private void OnCollisionExit2D(Collision2D other) {
        grounded = false;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Killer" || other.tag == "Ball"){
            audioSource.Play();
            StartCoroutine(Die());
            }
        else if(other.tag == "esc")
        {
            SceneManager.LoadScene("Win");
            PlayerPrefs.SetFloat("tiempoMuerte",gameTime);
        }
    }

    void SetTime(){
        int minutos, segundos;

        minutos = (int)(gameTime / 60f);
        segundos = (int)(gameTime % 60f);

        timer.text = minutos.ToString("00") + ":" + segundos.ToString("00");
    }
    

    IEnumerator Die(){

        anim.SetTrigger("dead");

        dead = true;

        lives--;

        yield return new WaitForSeconds(1.0f);

        graphic.SetActive(false);
        collider.enabled = false;

        yield return new WaitForSeconds(1.0f);
        if (lives > 0)
        {
            StartCoroutine(Parpadeo());
        }


        if (lives == 0)
        {
            SceneManager.LoadScene("die");
            PlayerPrefs.SetFloat("tiempoMuerte",gameTime);
        }
        else if (lives == 2)
        {
            vida3.SetActive(false);
            transform.position = new Vector3(-475, 12, 0);
        }
        else if (lives == 1)
        {
            vida2.SetActive(false);
            transform.position = new Vector3(-475, 12, 0);
        }
        else if (lives == 0)
        {
            vida1.SetActive(false);
            transform.position = new Vector3(-475, 12, 0);
        }
        collider.enabled = true;

    }
    IEnumerator Parpadeo()
    {
        dead = false;
        graphic.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        graphic.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        graphic.SetActive(true);
        graphic.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        graphic.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        graphic.SetActive(true);

        collider.enabled = true;
    }






}
