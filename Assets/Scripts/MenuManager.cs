﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        SceneManager.LoadScene("SampleScene");
    }
    public void PulsaQuit(){
        SceneManager.LoadScene("inicio");
    }
    
    public void PulsaExit(){
        Application.Quit();
    }
    public void PulsaCredits(){
        SceneManager.LoadScene("Credits");
    }
    

}

