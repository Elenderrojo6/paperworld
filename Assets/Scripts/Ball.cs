﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private float speedx;
    private float limitx;

    void Start(){
        speedx = 16f;
        limitx = -95f;
    }
    

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-speedx*Time.deltaTime,0,0);
        if (transform.position.x < limitx){
            Destroy(this.gameObject);
        }
    }

    
}
