﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 500f;

    private Rigidbody2D rb2d;

    void Start (){
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate (){
        rb2d.AddForce(Vector2.right * speed);
        transform.Translate(speed*Time.deltaTime,0,0);
        if (rb2d.velocity.x > -0.01f && rb2d.velocity.x < 0.01f){
            speed = -speed;
            rb2d.velocity = new Vector2(speed,rb2d.velocity.y);
        }

        if (speed > 0){
            transform.localScale = new Vector3(0.7f,0.7f,0.7f);
        }else if (speed < 0){
            transform.localScale = new Vector3(-0.7f,0.7f,0.7f);
        }
    }

    

}


