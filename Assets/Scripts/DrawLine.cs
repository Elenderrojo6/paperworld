﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{

    public Transform from;
    public Transform to;
    
    void OnDrawGizmosSelected(){
        if (from != null && to != null){
           Gizmos.color = Color.red; 
           Gizmos.DrawLine(from.position, to.position);
           Gizmos.DrawSphere(from.position, 1f);
           Gizmos.DrawSphere(to.position, 1f);
        }
    }
}
